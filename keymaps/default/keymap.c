#include "khingkhat.h"


/**
 * Layers
 */
#define _GENERIC 0
#define _DOTA 1
#define _MMO 2
#define _SYS 3
#define _NAV 4
#define _DOTAFN 5
#define _FN 19
#define _LAYERS 20

#define _____ KC_TRNS
#define XXXXX KC_NO


#define MEEPO2 0 //2 meepos
#define MEEPO3 1 //3 meepos
#define MEEPO4 2 //4 meepos
#define MEEPO5 3 //5 meepos
#define INVKR 4 
#define GHWLK 5 

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [_GENERIC] = {/* Base */
        {KC_GRV,  KC_1,    KC_2,    KC_3,     KC_4, KC_5},
        {KC_TAB,  KC_Q,    KC_W,    KC_E,     KC_R, KC_T, KC_Y },
        {KC_CAPS, KC_A,    KC_S,    KC_D,     KC_F, KC_G, KC_H },
        {KC_LSFT, KC_Z,    KC_X,    KC_C,     KC_V, KC_B, KC_N},
        {KC_LCTL, KC_LGUI, KC_LALT, KC_U,     KC_I },
        {KC_SPC,  KC_ENT,  KC_BSPC, MO(_FN),  MO(_LAYERS)} //thumb cluster
    },
    [_DOTA] = {/* dota */
        {KC_1,  KC_F1,  KC_2,    KC_3,     KC_4,  KC_F2},
        {_____, _____, _____,   _____,    _____, _____, _____ },
        {_____, _____, _____,   _____,    _____, _____, _____ },
        {_____, _____, KC_LALT, _____,    _____, _____, _____ },
        {_____, KC_NO, _____,   _____,    KC_GRV },
        {_____, KC_V,  KC_LALT, MO(_DOTAFN),  MO(_LAYERS) } //thumb cluster
    },
    [_MMO] = {/* MMO */
        {_____, _____, _____, _____,    _____, _____},
        {_____, _____, _____, _____,    _____, _____, _____ },
        {_____, _____, _____, _____,    _____, _____, _____ },
        {_____, _____, _____, _____,    _____, _____, _____ },
        {_____, _____, _____, _____,    _____ },
        {_____, _____, _____, MO(_FN),  MO(_LAYERS) } //thumb cluster
    },
    [_SYS] = {/* SYSTEM CONTROLS */
        {KC_ESC,  XXXXX,   XXXXX,   XXXXX,    XXXXX, RESET},
        {KC_VOLU, KC_MPRV, KC_MPLY, KC_MNXT,  XXXXX, XXXXX, KC_PWR },
        {KC_MUTE, XXXXX,   XXXXX,   XXXXX,    XXXXX, XXXXX, KC_SLEP },
        {KC_VOLD, XXXXX,   XXXXX,   XXXXX,    XXXXX, XXXXX, KC_WAKE },
        {RGB_TOG, RGB_HUI, RGB_SAI, RGB_VAI,  RGB_VAD },
        {RGB_MOD, RGB_HUD, RGB_SAD, MO(_FN),  MO(_LAYERS) } //thumb cluster
    },
    [_FN] = {/* Function */
        {KC_ESC, KC_F1,  KC_F2,  KC_F3,    KC_F4, KC_F5},
        {_____,  KC_F6,  KC_F7,  KC_F8,    KC_F9, KC_F10, KC_PSCR },
        {_____,  KC_F11, KC_F12, _____,    _____, _____, _____ },
        {_____,  _____,  _____,  _____,    _____, _____, _____ },
        {_____,  _____,  _____,  _____,    _____ },
        {_____,  _____,  _____,  _____,    XXXXX } //thumb cluster
    },
    [_NAV] = {/* Navigation */
        {_____, KC_HOME, _____,   KC_END,   _____, _____},
        {_____, KC_PGUP, KC_UP,   KC_PGDN,  _____, _____, _____ },
        {_____, KC_LEFT, KC_DOWN, KC_RGHT,  _____, _____, _____ },
        {_____, _____,   _____,   _____,    _____, _____, _____ },
        {_____, _____,   _____,   _____,    _____ },
        {_____, _____,   _____,   MO(_FN),  MO(_LAYERS) } //thumb cluster
    },
    [_DOTAFN] = {/* MMO */
        {_____,     _____,     _____,     _____, M(INVKR), M(GHWLK)},
        {_____,     _____,     _____,     _____, _____,    KC_VOLU, KC_MPRV },
        {_____,     _____,     _____,     _____, _____,    KC_MUTE, KC_MPLY },
        {_____,     _____,     _____,     _____, _____,    KC_VOLD, KC_MNXT },
        {_____,     _____,     _____,     _____, _____,     _____, _____ },
        {M(MEEPO3), M(MEEPO4), M(MEEPO5), _____, MO(_LAYERS) } //thumb cluster
    },
    [_LAYERS] = {/* Layer Selection */
        {DF(_GENERIC), XXXXX, XXXXX, XXXXX,    XXXXX, XXXXX },
        {DF(_NAV),     XXXXX, XXXXX, XXXXX,    XXXXX, XXXXX, DF(_DOTA) } ,
        {XXXXX,        XXXXX, XXXXX, XXXXX,    XXXXX, XXXXX, DF(_MMO) } ,
        {XXXXX,        XXXXX, XXXXX, XXXXX,    XXXXX, XXXXX, DF(_SYS) } ,
        {XXXXX,        XXXXX, XXXXX, XXXXX,    XXXXX },
        {XXXXX,        XXXXX, XXXXX, XXXXX,  _____ } //thumb cluster
    }
};

const uint16_t PROGMEM fn_actions[] = {

};

const macro_t *action_get_macro(keyrecord_t *record, uint8_t id, uint8_t opt)
{
    // MACRODOWN only works in this function
    switch(id) {
        case MEEPO2:
            if (record->event.pressed) {
                return MACRO( I(100), T(2), T(W), T(2), END);
            }
        case MEEPO3:
            if (record->event.pressed) {
                return MACRO( I(100), T(2), T(W), T(2), T(W), T(2), END);
            }
        case MEEPO4:
            if (record->event.pressed) {
                return MACRO( I(100), T(2), T(W), T(2), T(W), T(2), T(W), T(2), END);
            }
        case MEEPO5:
            if (record->event.pressed) {
                return MACRO( I(100), T(2), T(W), T(2), T(W), T(2), T(W), T(2), T(W), T(2), END);
            }
        case INVKR:
            if (record->event.pressed) {
                return MACRO( I(100), T(F), T(D), T(Q), T(W), T(E), T(R), W(200), END);
            }
        case GHWLK:
            if (record->event.pressed) {
                return MACRO( I(100), T(Q), T(Q), T(W), T(R), W(200), T(D), END);
            }
        break;
    }
    return MACRO_NONE;
};


void matrix_init_user(void) {

}

void matrix_scan_user(void) {

}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    return true;
}

void led_set_user(uint8_t usb_led) {

}
