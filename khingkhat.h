#ifndef KHINGKHAT_H
#define KHINGKHAT_H

#include "quantum.h"

// This a shortcut to help you visually see your layout.
// The following is an example using the Planck MIT layout
// The first section contains all of the arguements
// The second converts the arguments into a two-dimensional array
#define KEYMAP( \
	k00, k01, k02, k03, k04, k05,\
	k10, k11, k12, k13, k14, k15,   k16, \
	k20, k21, k22, k23, k24, k25,   k26, \
	k30, k31, k32, k33, k34, k35,   k36, \
	k40, k41, k42, k43, k44, \
	k50, k51, k52, k53, k54 \
) \
{ \
    { k00, k01, k02, k03, k04, k05,   KC_NO }, \
    { k10, k11, k12, k13, k14, k15,   k16 }, \
    { k20, k21, k22, k23, k24, k25,   k26 }, \
    { k30, k31, k32, k33, k34, k35,   k36 }, \
    { k40, k41, k42, k43, k44, KC_NO, KC_NO}, \
    { k50, k51, k52, k53, k54, KC_NO, KC_NO}, \
}

#endif
